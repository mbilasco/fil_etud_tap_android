package com.takeaphoto.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.takeaphoto.model.UserTwitter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

public class Login_twitter extends Activity implements View.OnClickListener {

    public static final String PREF_NAME = "sample_twitter_pref";
    public static final String PREF_KEY_OAUTH_TOKEN = "oauth_token";
    public static final String PREF_KEY_OAUTH_SECRET = "oauth_token_secret";
    public static final String PREF_KEY_TWITTER_LOGIN = "is_twitter_loggedin";
    public static final String PREF_USER_NAME = "twitter_user_name";

    public static final int WEB_REQUEST_CODE = 100;

    public static ProgressDialog pDialog;

    private static Twitter twitter;
    private static RequestToken resquestToken;

    private static SharedPreferences sharedPreferences;

    private EditText shareEditText;
    private TextView userName;
    private View loginLayout;
    private View shareLayout;

    private String consumerKey = null;
    private String consumerSecret = null;
    private String callbackUrl = null;
    private String oAuthVerifier = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initTwitterConfigs();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.activity_login_twitter);

        loginLayout = (RelativeLayout) findViewById(R.id.login_layout);

        findViewById(R.id.btn_login).setOnClickListener(this);

        if (TextUtils.isEmpty(consumerKey) || TextUtils.isEmpty(consumerSecret)) {
            Toast.makeText(this, "Twitter key or secret not configured", Toast.LENGTH_LONG).show();

            return;
        }

        sharedPreferences = getSharedPreferences(PREF_NAME, 0);

        boolean isLoggedIn = sharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN, false);

        if (isLoggedIn) {

            String username = sharedPreferences.getString(PREF_USER_NAME, "");

            Toast.makeText(getApplicationContext(), "Vous êtes connecté à Twitter " + username, Toast.LENGTH_SHORT).show();

        } else {
            Uri uri = getIntent().getData();

            if (uri != null && uri.toString().startsWith(callbackUrl)) {

                String verifier = uri.getQueryParameter(oAuthVerifier);

                try {

                    AccessToken accessToken = twitter.getOAuthAccessToken(resquestToken, verifier);
                    long userId = accessToken.getUserId();
                    final User user = twitter.showUser(userId);
                    final String username = user.getName();

                    saveTwitterInfo(accessToken);
                    userName.setText(username);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    private void initTwitterConfigs() {
        consumerKey = getString(R.string.twitter_consumer_key);
        consumerSecret = getString(R.string.twitter_consumer_secret);
        callbackUrl = getString(R.string.twitter_callback);
        oAuthVerifier = getString(R.string.twitter_oauth_verifier);
    }

    public void launchActivity() {
        Intent intent = new Intent(getApplicationContext(), FlickrActivity.class);
        startActivity(intent);
        finish();
    }


    private void saveTwitterInfo(AccessToken accessToken) {

        long userId = accessToken.getUserId();

        User user;

        try {

            user = twitter.showUser(userId);
            String username = user.getName();

            SharedPreferences.Editor e = sharedPreferences.edit();
            e.putString(PREF_KEY_OAUTH_TOKEN, accessToken.getToken());
            e.putString(PREF_KEY_OAUTH_SECRET, accessToken.getTokenSecret());
            e.putBoolean(PREF_KEY_TWITTER_LOGIN, true);
            e.putString(PREF_USER_NAME, username);
            e.commit();


        } catch (TwitterException e) {
            e.printStackTrace();
        }
    }

    private void loginToTwitter() {

        boolean isLoggedIn = sharedPreferences.getBoolean(PREF_KEY_TWITTER_LOGIN, false);

        if (!isLoggedIn) {
            final ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.setOAuthConsumerKey(consumerKey);
            builder.setOAuthConsumerSecret(consumerSecret);

            final Configuration configuration = builder.build();
            final TwitterFactory factory = new TwitterFactory(configuration);
            twitter = factory.getInstance();

            try {
                resquestToken = twitter.getOAuthRequestToken(callbackUrl);

                final Intent intent = new Intent(this, WebViewTwitter.class);
                intent.putExtra(WebViewTwitter.EXTRA_URL, resquestToken.getAuthenticationURL());
                startActivityForResult(intent, WEB_REQUEST_CODE);

            } catch (TwitterException e) {
                e.printStackTrace();
            }
        } else {
            launchActivity();
        }

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {

            final String verifier = data.getExtras().getString(oAuthVerifier);

            try {
                AccessToken accessToken = twitter.getOAuthAccessToken(resquestToken, verifier);
                final String name = accessToken.getScreenName();
                saveTwitterInfo(accessToken);

                long userId = accessToken.getUserId();
                final User user = twitter.showUser(userId);
                String username = user.getName();
                saveTwitterInfo(accessToken);
                launchActivity();

            } catch (TwitterException exception) {
                exception.printStackTrace();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                loginToTwitter();
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
    }


    public static ProgressDialog getpDialog() {
        return pDialog;
    }

    public static class updateTwitterStatus extends AsyncTask<String, String, Void> {

        Activity mActivity;
        File photoFile;
        String status;

        public updateTwitterStatus (Activity activity, File file, String statusTweet)
        {
            super();
            mActivity = activity;
            photoFile = file;
            status = statusTweet;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(mActivity);
            pDialog.setMessage("Posting to Twitter...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {

                //status += "  @TAP_TakeAPhoto  #TAP2016ReponseALDemande";

            try {
                ConfigurationBuilder builder = new ConfigurationBuilder();
                builder.setOAuthConsumerKey("6474DI257w6xyUzEjZb4ivkyd");
                builder.setOAuthConsumerSecret("lbfI9Lq7Rpf9Fi1yKxJE58IxdMX1RP7YONtjdcStIavFhEYAf1");

                String acces_token = sharedPreferences.getString(PREF_KEY_OAUTH_TOKEN, "");
                String acce_token_secret = sharedPreferences.getString(PREF_KEY_OAUTH_SECRET, "");

                AccessToken accessToken = new AccessToken(acces_token, acce_token_secret);

                Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

                StatusUpdate statusUpdate = new StatusUpdate(status);
                //InputStream is = getResources().openRawResource(+R.mipmap.landscape);
                FileInputStream f = new FileInputStream(photoFile);
                statusUpdate.setMedia("Photo form take a photo", f);

                twitter4j.Status response = twitter.updateStatus(statusUpdate);

            } catch (TwitterException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            pDialog.dismiss();

            Toast.makeText(mActivity, "Posted to Twitter!", Toast.LENGTH_SHORT);
           // shareEditText.setText("");
        }
    }
}



