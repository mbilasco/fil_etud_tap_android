package com.takeaphoto.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.googlecode.flickrjandroid.FlickrException;
import com.googlecode.flickrjandroid.RequestContext;
import com.googlecode.flickrjandroid.oauth.OAuth;
import com.googlecode.flickrjandroid.uploader.UploadMetaData;
import com.googlecode.flickrjandroid.uploader.Uploader;
import com.takeaphoto.flickr.FlickrHelper;
import com.takeaphoto.flickr.UploadPhotoFlickr;
import com.takeaphoto.model.Demande;
import com.takeaphoto.model.User;
import com.takeaphoto.server.DemandeServeur;

import org.xml.sax.SAXException;

import twitter4j.StatusUpdate;

/**
 * Fragment permettant d'ajouter et visualiser ses demandes par des markers sur une map 
 * @author Maxime & Jules
 *
 */
public class MapAdd extends SupportMapFragment implements OnMarkerDragListener {

    static final int REQUEST_TAKE_PHOTO = 1;

	private GoogleMap gMap;
	private MarkerOptions ajoutMarker;
	private Activity mainActivity;
	private User user;
	private ArrayList<Demande> demandes;
    private String description;
    private OAuth oauth;
    private File photoFile;
    private ProgressDialog mProgressDialog;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ajoutMarker = new MarkerOptions();
		setHasOptionsMenu(true);
	}

	public void initialize(Activity main, User user) {
		this.setMainActivity(main);
		this.setUser(user);
	}

	public void setMainActivity(Activity main) {
		mainActivity = main;
	}
	
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Ajout des demandes représentés par des markers sur la map
	 */
	private void setMarkerDemandes() {
		MarkerOptions m = new MarkerOptions();
		
		for(Demande d : demandes){
			m.title(d.getDescription());
			
			switch(d.getEtat()){
				case 0 :
					m.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
					break;
				case 1 :
					m.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
					break;
				case 2 :
					m.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
					break;
			}
			m.position(new LatLng(d.getLat(), d.getLng()));
			m.snippet(d.getDescription()+"");
			m.draggable(true);
	
			gMap.addMarker(m);
		}
	}
	
	/**
	 * Ajout d'un marker lors de l'ajout d'une demande
	 * @param result
	 */
	private void setMarker(String result) {
		ajoutMarker.title("Demande non validée");
		ajoutMarker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
		ajoutMarker.draggable(true);
		ajoutMarker.snippet(result);
		gMap.addMarker(ajoutMarker);
	}

	@Override
	public void onResume() {
		super.onResume();
		setUpMapIfNeeded();
		gMap.clear();
		setMarkerDemandes();
	}

	private void setUpMapIfNeeded() {
		if (gMap == null) {
			gMap = getMap();
			gMap.setMyLocationEnabled(true);
			gMap.setOnMarkerDragListener(this);
			LatLng position = new LatLng(42.00,24.00);
	        gMap.moveCamera(CameraUpdateFactory.newLatLng(position));
	        gMap.animateCamera(CameraUpdateFactory.zoomTo(2));
			
			gMap.setOnMapLongClickListener(new OnMapLongClickListener() {
				public void onMapLongClick(final LatLng point) {
					ajoutMarker.position(point);

					AlertDialog.Builder alert = new AlertDialog.Builder(mainActivity);

					alert.setTitle("Description de la photo voulue :");

					final EditText input = new EditText(mainActivity);
					alert.setView(input);

					alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int whichButton) {
							setMarker(input.getText().toString());
                            description = "{ "+input.getText().toString()+" "+point +" }";
                            prendrePhoto();
						}
					});

					alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int whichButton) {	}
					});

					alert.show();

					gMap.animateCamera(CameraUpdateFactory.newLatLng(point));

				}
			});
		}
	}

	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.map_add, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menu_save:
				String desc = ajoutMarker.getSnippet();
				AlertDialog.Builder alert = new AlertDialog.Builder(mainActivity);
	
				if (desc != null) {
					alert.setTitle("Voulez-vous vraiment valider cette demande ?");
					alert.setMessage("Description : \n"	+ ajoutMarker.getSnippet());
					
					alert.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,	int whichButton) {

                                String longitude = String.valueOf(ajoutMarker.getPosition().longitude);
                                String latitude = String.valueOf(ajoutMarker.getPosition().latitude);
                                Demande demande = new Demande(user.getUserId(), ajoutMarker.getPosition().latitude, ajoutMarker.getPosition().longitude, ajoutMarker.getSnippet());

                                description = "{ Latitude : "+ajoutMarker.getPosition().latitude+" ,Longitude :"+ajoutMarker.getPosition().longitude+" , Description : "+ajoutMarker.getSnippet() + "} @TAP_TakeAPhoto  #TAP2016ReponseALDemande";
                                //posteImage(description);
                                UploadPhotoFlickr task = new UploadPhotoFlickr(getActivity(),oauth,photoFile,mProgressDialog,latitude,longitude);

                                DemandeServeur demandeServeur = new DemandeServeur();

                                demandeServeur.addDemande(user, demande);
                                demandes.add(demande);


                                Toast.makeText(mainActivity, "Votre demande a ete ajoutee", Toast.LENGTH_SHORT).show();

                            }

						});
	
					alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {	}
					});
				} else {
					alert.setTitle("Erreur");
					alert.setMessage("Vous devez d'abord poser un marqueur et lui ajouter une description");
					alert.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    });
				}
	
				alert.show();
				return true;
			default:
				return super.onOptionsItemSelected(item);
			}
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View myFragmentView = super.onCreateView(inflater, container, savedInstanceState);
		return myFragmentView;
	}

	@Override
	public void onMarkerDrag(Marker marker) {	}

	@Override
	public void onMarkerDragEnd(Marker marker) {
		if(marker.getTitle().compareTo("Demande non validée")!=0){
			for(Demande d : demandes){
				if(d.getId() == Integer.parseInt(marker.getSnippet())){
					d.setLat(marker.getPosition().latitude);
					d.setLng(marker.getPosition().longitude);
					DemandeServeur demandeServeur = new DemandeServeur();
					demandeServeur.updatePositionDemande(d.getId(), d.getLat(), d.getLng());
				}
			}
		}
		else{
			ajoutMarker.position(marker.getPosition());
		}
	}

	@Override
	public void onMarkerDragStart(Marker marker) {	  }

	public ArrayList<Demande> getDemandes() {
		return demandes;
	}

	public void setDemandes(ArrayList<Demande> demandes) {
		this.demandes = demandes;
	}

    public void setOauth(Serializable oauth) {
        this.oauth = (OAuth) oauth;
    }

    /*public  final void posteImage(String description)
	{

        String result = new String("debut upload");
        RequestContext.getRequestContext().setOAuth(oauth);
        try {
            Uploader uploader = new Uploader(FlickrHelper.API_KEY, FlickrHelper.API_SEC);
            UploadMetaData metaData = new UploadMetaData();
            Drawable d = getResources().getDrawable(R.drawable.photo_vide);
            // the drawable (Captain Obvious, to the rescue!!!)
            Bitmap bitmap = ((BitmapDrawable) d).getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] bitmapdata = stream.toByteArray();
            Log.i("Bitmap","Bitmap value= " + bitmapdata);
                metaData.setTitle("test");
                metaData.setDescription(description);
                metaData.setTags(new ArrayList<String>());
                //metaData.setPublicFlag(true);
                //metaData.setFriendFlag(true);
                //metaData.setFamilyFlag(true);
                //metaData.setHidden(false);
                //metaData.setSafetyLevel("normal");
                //metaData.setAsync(false); //true pour async
                metaData.setContentType("ma photo");

                uploader.upload("Photo form Take A photo", bitmapdata, metaData);
            } catch (FileNotFoundException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            } catch (SAXException e) {
            e.printStackTrace();
            } catch (FlickrException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


    }*/

    /*public byte[] RecupereImage()
    {
        int id = R.drawable.photo_vide;
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inScaled = false;
        Bitmap bitmap = BitmapFactory.decodeResource(mainActivity.getResources(),id,opts);
        //Bitmap bmpCompressed = Bitmap.createScaledBitmap(bitmap, 600, 400, true);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bos);
        byte[] image =bos.toByteArray();
        return image;


        //calculate how many bytes our image consists of.
        /*int bytes = bitmap.getByteCount();
        //or we can calculate bytes this way. Use a different value than 4 if you don't use 32bit images.
        //int bytes = b.getWidth()*b.getHeight()*4;
        ByteBuffer buffer = ByteBuffer.allocate(bytes); //Create a new buffer
        bitmap.copyPixelsToBuffer(buffer); //Move the byte data to the buffer


        byte[] array = buffer.array(); //Get the underlying array containing the data.

        return array;


    }*/

    public void prendrePhoto(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                System.err.print("IOException");
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        // Save a file: path for use with ACTION_VIEW intents
        String mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }
}
