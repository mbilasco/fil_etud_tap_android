package com.takeaphoto.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewTwitter extends Activity {

    private WebView webView;

    public static String EXTRA_URL = "extra_url";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webviewtwitter);
        Log.i("On entre oncreate", "On entre oncreate");
        final String url = this.getIntent().getStringExtra(EXTRA_URL );

        if(url == null){
            finish();
        }

        webView = (WebView) findViewById(R.id.weView);
        webView.setWebViewClient(new MyWebViewClient());
        Log.i("apres l'initialisation", "apres l'initialisation");
        webView.loadUrl(url);
        Log.i("fin", "fin");
    }


    class MyWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            if(url.contains(getResources().getString(R.string.twitter_callback))) {

                Uri uri = Uri.parse(url);
                String verifier = uri.getQueryParameter(getString(R.string.twitter_oauth_verifier));
                Intent resultIntent = new Intent();
                resultIntent.putExtra(getString(R.string.twitter_oauth_verifier),verifier);
                setResult(RESULT_OK, resultIntent);
                Log.i("dans le webclient", "dans le webclient");
                finish();
                return true;
            }

            return false;
        }
    }

}





